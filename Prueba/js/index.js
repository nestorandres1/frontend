console.log("Hola desde index.js")
document.getElementById("selectName").style.display = 'none'
//Variables
var estado = 1 + 3 + 'Hola'
console.log(estado)
let suma = 4 + 7 + true 
console.log(suma)
const PI = 3.1416
console.log(PI)

//Objeto - JSON
const user = {
    "id" : 0,
    "user" : "narodriguezg",
    "pass": "1234",
}
console.log(user)
//Array de objetos
const users = 
[
    {
        "id" : 1,
        "nombre" : "Jose",
        "password": "1234",
    },
    {
        "id" : 2,
        "nombre" : "Guillermo",
        "password": "12345",
    },
    {
        "id" : 3,
        "nombre" : "Florinda",
        "password": "123",
    }
]
console.log(users)

//Condicionales
//En una condición debe existir:
// 1. Variable
// 2. Operador lógico
// 3. valor
let numero = 3
if (numero > 0) {
    console.log("El numero es mayor que cero")
}

//ciclos
for (let i = 0; i < users.length; i++) {
    const element = users[i]
    console.log(element)
    if(element.id == 1)
        console.log("Bienvenido "+element.nombre)
    if(element.id == 2)
        console.log("Bienvenido "+element.nombre)
    if(element.id == 3)
        console.log("Bienvenido "+element.nombre)
}


//Funciones ES6
const login =  () => {
    console.log("Hola desde login")
    let userLogin ={
        "user": "",
        "pass": ""
    }
    userLogin.user = document.formLogin.username.value
    userLogin.pass = document.formLogin.password.value
    //or lógico
    if(userLogin.user == '' || userLogin.pass == '')
        alert("Los campos son obligatorios")
    else
        validar(userLogin)
}
//ES6 fetch para consumir API
const validar = (userLogin) => {
    document.getElementById("selectName").style.display=''
    console.log(userLogin)
    let url = 'https://jsonplaceholder.typicode.com/comments/'
    //and lógica
    if(userLogin.user == users[0].nombre && userLogin.pass == users[0].password){
        fetch(url, {
            method: 'POST', // or 'PUT'
            body: JSON.stringify(userLogin), // data can be `string` or {object}!
            headers:{
                'Content-Type': 'application/json'
            }
        }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => console.log('Success:', response));
    }
    else{
        console.log("Usted no es un usuario registrado")
    }
}

let select = document.getElementById("selectName")

for(let i = 0; i < users.length; i++) {
    let opt = users[i];
    let el = document.createElement("option");
    el.textContent = opt.nombre;
    el.value = opt.id;
    select.appendChild(el);
}

